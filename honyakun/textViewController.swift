//
//  textViewController.swift
//  honyakun
//
//  Created by WKC on 2016/09/19.
//  Copyright © 2016年 WKC. All rights reserved.
//

import Foundation


import UIKit
import NendAd

class textViewController: UIViewController,UIWebViewDelegate,popWordProt,touchImageViewProtocol,cameraHttpProtocol,NADViewDelegate {
    
    //画像
    var im:UIImage!
    var crip:Int!
    
    //net周辺
   // var net = Net(baseUrlString: "http://153.120.62.197/cruster/tesorgNEO.php")
    
    //ナビゲーションバー高さ
    var barHeight:CGFloat!
    
    //webView
    var ocrView:UIWebView!
    var webText:String! = ""
    var htmlContents:String!
    
    //言語
    var lang:String!
    
    //単語カード
    var wordView:popWord!
    
    //保存、リクエスト周辺
    var TouchedWord = ""
    
    //swiftData
   // var _mySwiftData = MySwiftData()
    
    //発音クラス
    var sayClass:SayClass!
    
    //タイトル
    var textTitle:String!
    
    //文書画像周り
    var textImageView:touchImageView!
    var textImageButton:UIBarButtonItem!
    var textImageViewFlag:Bool = false
    
    //nend広告
    var nadView:NADView!
    
    //db
    var worddb = wordDB.accessDB//wordDB()

    var cameraRequest:CameraRequestAPI!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        //self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0xFFFFF3)
        self.setSubView()
        self.setWebView()
        self.setnavBar()
        self.setCameraRequest()
        adNend()
    }
    
    func setCameraRequest(){
        cameraRequest = CameraRequestAPI(lang: lang)
        cameraRequest.delegate = self
    }
    func setSubView(){        
        //発音クラス初期化
        sayClass = SayClass(lang:lang)
        //webView
        self.barHeight = self.navigationController?.navigationBar.frame.size.height
        ocrView = UIWebView(frame:CGRect(x: 0,y: barHeight,width: self.view.frame.size.width,height: self.view.frame.size.height-barHeight))
        ocrView.delegate=self
        ocrView.backgroundColor=UIColorFromRGB(0xFFFFFF)
        self.view.addSubview(ocrView)
        
        //単語View
        wordView = popWord(frame: CGRect(x: 0,y: self.view.frame.size.height+40,width: self.view.frame.size.width,height: 200),word: "",text: "",lang:lang)
        wordView.delegate = self
        self.view.addSubview(wordView)
        
    }
    
    func setWebView(){
        self.ocrView.loadHTMLString(webText, baseURL: nil)
    }
    
    func setnavBar(){
        let titleView = UILabel(frame:CGRect.zero)
        titleView.font = UIFont.boldSystemFont(ofSize: 14.0)
        titleView.textColor = UIColorFromRGB(0xFF5F5C)
        titleView.text = textTitle
        titleView.sizeToFit()
        self.navigationItem.titleView = titleView
        
        //navButton
        let size = CGSize(width: 20, height: 20)
        let tmpImage = UIImage(named: "new-document.png")
        let textImage = UIImage(named: "album.png")
        
        let navDocImage = resizeImage(tmpImage!,size: size)
        let textRImage = resizeImage(textImage!, size: size)
        //文書画像ボタン
        textImageButton = UIBarButtonItem(image:textRImage, style: UIBarButtonItemStyle.plain, target: self, action: #selector(textViewController.slideImage))
        self.navigationItem.setRightBarButton(textImageButton, animated:true)
        //戻るボタン
       
        /*var backButton = UIBarButtonItem(image:resizeImage(UIImage(named:"la.png")!,size: CGSize(width: 30, height: 30)),style:UIBarButtonItemStyle.plain, target: self, action: #selector(textViewController.back))
        self.navigationItem.setLeftBarButton(backButton, animated: true)*/
        //self.navigationItem.backBarButtonItem?.image = UIImage(named:"la.png")
        //文書画像View
        textImageView = touchImageView(frame:CGRect(x: self.view.frame.size.width,y: (self.navigationController?.navigationBar.frame.size.height)!,width: self.view.frame.size.width,height: self.view.frame.size.height))
        textImageView.delegates = self
        textImageView.isUserInteractionEnabled = true
        textImageView.image = self.im
        self.view.addSubview(textImageView)
    }
    
    func back(){
        self.navigationController?.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
        //self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//単語周り
extension textViewController{

    func dawnView() {
        wordView.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height+100)
    }
    
    func wordPreserve(_ word:String,text:String) {
        worddb.saveWord(wordw: word, mean: text, lang: lang)
        SVProgressHUD.showSuccess(withStatus: "追加しました!")
    }

        
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
            SVProgressHUD.dismiss()
            
            let kScheme = "native://";
            let url = request.url!.absoluteString
            //  println("aiueo")
            if url.hasPrefix(kScheme) {
                //dealRequest(request)
                cameraRequest.dealRequest(request)
                return false  // ページ遷移を行わないようにfalseを返す
        }
            
            
            return true
        }
    
    
    func slideImage(){
        popImageView()
    }
    
    
    func popImageView(){
        if(self.textImageViewFlag == false){
            //アニメーション
            UIView.animate(withDuration: 0.5,
                                       
                                       // 遅延時間.
                delay: 0.0,
                
                // バネの弾性力. 小さいほど弾性力は大きくなる.
                usingSpringWithDamping: 0.8,
                
                // 初速度.
                initialSpringVelocity: 0.5,
                
                // 一定の速度.
                options: UIViewAnimationOptions.curveLinear,
                
                animations: { () -> Void in
                    
                    self.textImageView.layer.position = CGPoint(x: self.view.frame.size.width/2+80, y: (self.navigationController?.navigationBar.frame.size.height)!+self.view.frame.size.height/2)
                    self.textImageViewFlag = true
                    
                    // アニメーション完了時の処理
            }) { (Bool) -> Void in
                
            }
        }else{
            
            self.textImageView.layer.position = CGPoint(x: self.view.frame.size.width+2000, y: (self.navigationController?.navigationBar.frame.size.height)!+self.view.frame.size.height/2);
            self.textImageViewFlag = false
        }
    }
    
    func textImageTouched() {
        popImageView()
    }
    
    
    func resizeImage(_ image:UIImage,size:CGSize)->UIImage{
        
        UIGraphicsBeginImageContextWithOptions(size,false,0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let resizeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resizeImage!
        
    }

}




//http周り
extension textViewController{
    func popWordView(word:String,meanText text:String,type:String){
        adShowView()
        self.wordView.setProperty(word, text: text)
        //アニメーション
        UIView.animate(withDuration: 0.5,
                       
                       // 遅延時間.
            delay: 0.0,
            
            // バネの弾性力. 小さいほど弾性力は大きくなる.
            usingSpringWithDamping: 0.7,
            
            // 初速度.
            initialSpringVelocity: 0.5,
            
            // 一定の速度.
            options: UIViewAnimationOptions.curveLinear,
            
            animations: { () -> Void in
                
                self.wordView.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-100)
                
                // アニメーション完了時の処理
        }) { (Bool) -> Void in
            
        }
        
    }
    
    
    func showLoading(type: String) {
        if(type=="word"){
            SVProgressHUD.show(withStatus: "単語取得中")
        }else{
            SVProgressHUD.show(withStatus: "翻訳中")
        }
    }
    
    func deleteLoading() {
        SVProgressHUD.dismiss()
    }
}

extension textViewController{
    func adNend(){
        var magnification = self.view.frame.size.width / 320
        magnification = 50 * magnification
        // 広告枠作成
        nadView = NADView(frame: CGRect(x:0, y:self.view.frame.size.height - magnification , width:self.view.frame.size.width ,height:magnification), isAdjustAdSize: true)
        // 広告枠のapikey/spotidを設定(必須)
        //nadView.setNendID("a6eca9dd074372c898dd1df549301f277c53f2b9",spotID: "3172")
        nadView.setNendID("5c33a84a69b8bfd8145ccf55efd0e2547c03b2a1",spotID: "745795")
        // nendSDKログ出力の設定(任意)
        nadView.isOutputLog = false
        // delegateを受けるオブジェクトを指定(必須)
        nadView.delegate = self
        // 読み込み開始(必須)
        nadView.load()
        // 通知有無にかかわらずViewに乗せる場合
        // self.view.addSubview(nadView)
    }
    
    func adShowView(){
        var adCount = Int(arc4random_uniform(100))
        if(adCount > 30){
            nadView.pause()
            // nadView.layer.position = CGPoint(x:0,y:self.view.frame.size.height+100)
            nadView.removeFromSuperview()
            // nadView.isHidden = true
        }else{
            var magnification = self.view.frame.size.width / 320
            magnification = 50 * magnification
            //nadView.layer.position = CGPoint(x:0,y:self.view.frame.size.height - magnification)
            //nadView.
            // nadView = ::NADView(frame: CGRect(x:0, y:self.view.frame.size.height - magnification , width:self.view.frame.size.width ,height:magnification), isAdjustAdSize: true)
            nadView.resume()
            self.view.addSubview(nadView)
        }
    }
}


