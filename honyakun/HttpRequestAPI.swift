//
//  HttpRequestAPI.swift
//  honyakun
//
//  Created by WKC on 2017/04/16.
//  Copyright © 2017年 WKC. All rights reserved.
//

import Foundation


protocol cameraHttpProtocol{
    func showLoading(type:String)
    func deleteLoading()
    func popWordView(word:String,meanText:String,type:String)
}

class CameraRequestAPI:NSObject{
    
    
    var delegate:cameraHttpProtocol?
    
    let pattern = "\\<\\/br\\>|\\\n|\\<br\\>|br\\>";
    let replace=""
    
    var lang = ""
    var RegContents = ""
    init(lang:String){
        self.lang = lang
    }
    
    func dealRequest(_ request:URLRequest){
        let type = { () -> String in
            if(request.url!.host=="requestGo"){
                return "word"
            }else{
                return "text"
            }
        }()
        delegate?.showLoading(type:type)
        requestString(request.url!.path,type: type)
    }
    
    
    
    
    func requestString(_ url:String,type:String){
        
        
        let modifiedText = modifiedStringGenerator(url: url)
        let myUrl:URL = { () -> URL in
            if(type=="word"){
                return URL(string:"http://153.126.180.38/camerakun/api/WordGet/parameters?word=\(modifiedText)&lang=\(self.lang)")!
            }else{
                return URL(string:"http://153.126.180.38/camerakun/api/TextTranslate/parameters?text=\(modifiedText)&lang=\(self.lang)")!
            }
        }()
        
        
        //httpリクエスト
        let myRequest:NSMutableURLRequest  = NSMutableURLRequest(url: myUrl)
        myRequest.httpMethod="GET"
        if modifiedText != "" {
            NSURLConnection.sendAsynchronousRequest(myRequest as URLRequest, queue: OperationQueue.main) { (response, data, erroe) in
                if let _data = data {
                    self.getContents(data: data,modifiedWord: modifiedText,type:type)
                }
            }
        }else{
           delegate?.deleteLoading()
        }
    }
    
    
    
    func getContents(data:Data?,modifiedWord:String,type:String){
        // 帰ってきたデータを文字列に変換.
        
        let json = jsonClass(data: data!)
        let dataString = json.mean+"\n\n例文\n"+json.exText+"\n"+json.transText+"\n"
        var myData:NSString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
        delegate?.deleteLoading()
        delegate?.popWordView(word: RegContents, meanText:dataString,type:type)
    }

    
    
    func modifiedStringGenerator(url:String)->String{
        let urls:[String] = url.components(separatedBy: "/")
        let word = urls[urls.count-1]
        //正規表現で邪魔な記号をとる
        let RedWord = word.replacingOccurrences(of: pattern, with:replace, options:NSString.CompareOptions.regularExpression, range:nil)
        self.RegContents = RedWord
        let encodedString = CFURLCreateStringByAddingPercentEscapes(
            nil,
            RedWord as CFString!,
            nil,
            "!*'();:@&=+$,/?%#[]" as CFString!,
            CFStringBuiltInEncodings.UTF8.rawValue)
        print("エンコードもじ=>\(encodedString)")
        return encodedString as! String
        
    }


    
    
    
}
