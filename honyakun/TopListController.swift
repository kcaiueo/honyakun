//
//  topListController.swift
//  honyakun
//
//  Created by WKC on 2017/04/01.
//  Copyright © 2017年 WKC. All rights reserved.
//

import Foundation
import UIKit
import NendAd

class TopListController:UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,textCollProtocol{
    //テキスト情報(パスも含む)
    var textList = [Int:textSet]()
    var filePathNameList:[Int:String] = [:]
    var pathIndexList = [String:Int]()
    //CollectionView
    var rirekiCollectView:UICollectionView!
    //下側view
    var underView:UIView!
    //db
    var imagedb = imageDB.accessDB
    var orgdb = orgTextDB.accessDB
    //パスetc..
    var imagePath: String {
        let doc = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        return doc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adShow()
        self.automaticallyAdjustsScrollViewInsets = false
        setCollectionView()
        setView()
    }
    
    func setTexts(){
        filePathNameList = [:]
        pathIndexList = [:]
        textList = [:]
        let manager = FileManager.default
        var imageList:NSArray=[]
        do{
            imageList = try manager.contentsOfDirectory(atPath: imagePath) as NSArray
        }catch{
            print("miss")
        }
        
        var i = 0
        for path in imageList {
            let filepath:String! = URL(fileURLWithPath:imagePath).appendingPathComponent(path as! String).path
            let image = UIImage(contentsOfFile: filepath as String)
            let title = getTitle(path as! String)
            let lang = getLang(path as! String)
            var conText = orgdb.getImage(name: path as! String)
            if title == ""{ continue }
            filePathNameList[i] = path as! String
            pathIndexList[path as! String] = i
            textList[i] = textSet(title: title,image: image!,lang:lang,contentText: conText,path:path as! String)
            i += 1
        }
    }
    
    
    func setView(){
        self.navigationItem.title = "翻訳レンズ"
        let titleView = UILabel(frame:CGRect.zero)
        titleView.font = UIFont.boldSystemFont(ofSize: 14.0)
        titleView.textColor = UIColorFromRGB(0xFF5F5C)
        titleView.text = "翻訳レンズ"
        titleView.sizeToFit()
        self.navigationItem.titleView = titleView

        //下側
        underView = UIView(frame:CGRect(x: 0,y: self.view.frame.size.height-60,width: self.view.frame.size.width,height: 60))
        underView.backgroundColor = UIColorFromRGB(0xFFFFF3)//UIColorFromRGB(0xF8F8F8)
        //単語ボタン
        let underButton = UIButton(frame:CGRect(x:20,y:10,width:40,height:40))
        underButton.setImage(UIImage(named:"list.png"), for: .normal)
        underButton.addTarget(self, action: "showWordList", for: .touchUpInside)
        underView.addSubview(underButton)
        
        //検索ボタン
        let searchButton = UIButton(frame:CGRect(x:self.view.frame.size.width-60,y:10,width:40,height:40))
        searchButton.setImage(UIImage(named:"search.png"), for: .normal)
        searchButton.addTarget(self, action: "showSearch", for: .touchUpInside)
        underView.addSubview(searchButton)

        //カメラボタン
        let underCamera = UIButton(frame:CGRect(x:self.view.frame.size.width/2-25,y:self.view.frame.size.height-70,width:50,height:50))
        underCamera.setImage(UIImage(named:"rcamera.png"), for: .normal)
        underCamera.addTarget(self, action: "showCamera", for: .touchUpInside)
    
        let underCameraBack = UIButton(frame:CGRect(x:self.view.frame.size.width/2-35,y:self.view.frame.size.height-80,width:70,height:70))
        underCameraBack.backgroundColor = UIColorFromRGB(0xFFFFF3)
        underCameraBack.layer.cornerRadius = 30.0
        let underCamera2Back =  UIButton(frame:CGRect(x:self.view.frame.size.width/2-30,y:self.view.frame.size.height-75,width:60,height:60))
        underCamera2Back.backgroundColor = UIColorFromRGB(0xFF7F7C)
        underCamera2Back.layer.cornerRadius = 30.0

        self.view.addSubview(underView)
        self.view.addSubview(underCameraBack)
        self.view.addSubview(underCamera2Back)
        self.view.addSubview(underCamera)
    }
    
    
    func setCollectionView(){

        let layout = UICollectionViewFlowLayout()
        // Cell一つ一つの大きさ.
        layout.itemSize = CGSize(width:self.view.frame.size.width/2-10,height:self.view.frame.size.width/2-10)
        // Cellのマージン.
        layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5)
        // セクション毎のヘッダーサイズ.
        layout.headerReferenceSize = CGSize(width:10,height:10)
        // CollectionViewを生成.
        rirekiCollectView = UICollectionView(frame:CGRect(x:0,y:60,width:self.view.frame.size.width,height:self.view.frame.size.height-120), collectionViewLayout: layout)
        // Cellに使われるクラスを登録.
        // rirekiCollectView.backgroundColor = UIColorFromRGB(rgbValue: 0xF9FCC9)//UIColor.white
        rirekiCollectView.backgroundColor = UIColor.white
        let nib = UINib(nibName: "textCollCell", bundle: nil)
        rirekiCollectView.register(nib, forCellWithReuseIdentifier: "textCollCell")
        rirekiCollectView.delegate = self
        rirekiCollectView.dataSource = self
        self.view.addSubview(rirekiCollectView)
    }

    func getLang(_ path:String)->String{
        //let str = _mySwiftData.getLang(path)
        let str = imagedb.getLang(name: path)
        return str
    }
    
    func getTitle(_ path:String)->String{
        //let str = _mySwiftData.getTitle(path)
        let str = imagedb.getTitle(name: path)
        return str
    }
    
    func deleteText(path: String!) {
        deleteAndReset(path: path)
    }
    
    func deleteAndReset(path:String!){
        let ind = pathIndexList[path]
        let info = textList[ind!]
        imagedb.deleteImage(name: info?.path)
        self.setTexts()
        self.rirekiCollectView.reloadData()
    }
    
    func alert(text:String!){
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false,animated:true)
        //テキストボタン
        let underTButton = UIButton(frame:CGRect(x:20,y:0,width:40,height:40))
        underTButton.setImage(UIImage(named:"texts.png"), for: .normal)
        underTButton.addTarget(self, action: "showText", for: .touchUpInside)
        self.navigationController?.navigationBar.addSubview(underTButton)
        self.setTexts()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        for item in (self.navigationController?.navigationBar.subviews)!{
            if item is UIButton{
                item.removeFromSuperview()
            }
        }
        
    }
}


extension TopListController{
    
    func showCamera(){
        let controller = instantiate(cameraViewController.self,storyboard:"cameraViewController")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func showSearch(){
        let controller = instantiate(searchController.self,storyboard:"searchController")
        // controller.navigationController?.setNavigationBarHidden(true,animated:true)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func showText(){
        let controller = instantiate(textListController.self,storyboard:"textListController")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func showWordList(){
        let controller = instantiate(wordListController.self,storyboard:"wordListController")
        self.navigationController?.pushViewController(controller, animated: true)
    }
  }



extension TopListController{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var info = textList[indexPath.row]
        let kakotext = textViewController() as textViewController
        kakotext.lang = textList[indexPath.row]!.lang
        kakotext.textTitle = textList[indexPath.row]!.titleText
        kakotext.webText = imagedb.getImage(name: filePathNameList[indexPath.row]!)//textList[indexPath.row]?.contentText
        self.navigationController?.pushViewController(kakotext, animated: true)
    }
    /*
     Cellの総数を返す
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return textList.count
    }
    
    /*
     Cellに値を設定する
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "textCollCell", for: indexPath) as? textCollCell else{
            return UICollectionViewCell()
        }
        cell.setCell(textInfo: textList[indexPath.row]!)
        cell.delegate = self
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        // Util からでディスプレイサイズ取得
        // Width はそれを使い，高さは適切な値で返してやる
        let returnSize = CGSize(width:(self.view.frame.size.height-120)/3, height: (self.view.frame.size.height-120)/3-60)
        return returnSize
    }
    
    
}
