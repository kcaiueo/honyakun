//
//  FirstViewController.swift
//  honyakun
//
//  Created by WKC on 2017/02/26.
//  Copyright © 2017年 WKC. All rights reserved.
//

import Foundation
import UIKit
//import GoogleMobileAds
class firstViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    var underView:UIView!
    var camera:UIButton!
    var wordcamera:UIButton!
    var wordsearch:UIButton!
    var textrireki:UIButton!
    var wordcard:UIButton!
    
    
    //db
    var worddb = wordDB()
    var textdb = textDB()
    var imagedb = imageDB()
    var orgdb = orgTextDB()
    var iddb = IDCheckDB()
    
    
    
    //テーブル
    var contentTableView:UITableView!
    
    //コンテンツ
    var contentArray:[Int:topContent] = [:]
     override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        setView()
        setContent()
        setTableView()
    }
    
    func setContent(){
        contentArray[0] = topContent(imageName: "camera.png",contentTitle: "文章撮影")
        contentArray[1] = topContent(imageName: "research.png",contentTitle: "一文撮影")
        contentArray[2] = topContent(imageName: "search.png",contentTitle: "単語検索")
        contentArray[3] = topContent(imageName: "book.png",contentTitle: "保存文章")
        contentArray[4] = topContent(imageName: "wordCard",contentTitle: "単語履歴")
    }
    
    func setTableView(){
        self.contentTableView = UITableView(frame:CGRect(x:0,y:60,width:self.view.frame.size.width,height:self.view.frame.size.height-120))
        contentTableView.delegate = self
        contentTableView.dataSource = self
        contentTableView.separatorColor = UIColor.clear
        let nib = UINib(nibName: "topContentCell", bundle: nil)
        self.contentTableView.register(nib, forCellReuseIdentifier: "topContentCell")
        self.view.addSubview(contentTableView)
    }
    
    func setView(){
        underView = UIView(frame:CGRect(x: 0,y: self.view.frame.size.height-60,width: self.view.frame.size.width,height: 60))
        underView.backgroundColor = UIColorFromRGB(0xFFFFF3)//UIColorFromRGB(0xF8F8F8)
        self.view.addSubview(underView)
    }
    
    
    func setnavBar(){
        // タイトルを設定する.
        self.navigationItem.title = "翻訳レンズ"
        let titleView = UILabel(frame:CGRect.zero)
        titleView.font = UIFont.boldSystemFont(ofSize: 14.0)
        titleView.textColor = UIColorFromRGB(0xFF5BBE)
        titleView.text = "翻訳レンズ"
        titleView.sizeToFit()
        self.navigationItem.titleView = titleView
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}

extension firstViewController{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       /* var filePath = filePathNameList[indexPath.row]
        let storyboard = UIStoryboard(name: "textViewController", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "textViewController") as! textViewController
        //mainViewController.webText = _mySwiftData.getImage(filePathNameList[indexPath.row]!)
        mainViewController.webText = imagedb.getImage(name: filePathNameList[indexPath.row]!)
        mainViewController.lang = contentArray[indexPath.row]?.lang
        mainViewController.textTitle = contentArray[indexPath.row]?.title
        self.navigationController?.pushViewController(mainViewController, animated: true)*/
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return contentTableView.frame.size.height/5;//self.view.frame.size.height/6;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "topContentCell", for: indexPath) as? topContentCell else {
            return UITableViewCell()
        }
        for subview in cell.subviews{
            if(subview.className == "UILabel"){
                subview.removeFromSuperview()
            }
        }
        cell.setSelected(true, animated: true)
        
        cell.setCell(contentArray[indexPath.row]!)
        return cell
    }

    
      override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false,animated:true)
        //self.navigationController?.navigationBar.backgroundColor = UIColorFromRGB(0xF8F8F8)
    }
    
    
}



extension firstViewController{
    
    func showCamera(){
        let controller = instantiate(cameraViewController.self,storyboard:"cameraViewController")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func showWordCamera(){
        let controller = instantiate(wordOcrController.self,storyboard:"wordOcrController")
        // controller.navigationController?.setNavigationBarHidden(true,animated:true)
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func showSearch(){
        let controller = instantiate(searchController.self,storyboard:"searchController")
        // controller.navigationController?.setNavigationBarHidden(true,animated:true)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func showText(){
        let controller = instantiate(textListController.self,storyboard:"textListController")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func showWordList(){
        let controller = instantiate(wordListController.self,storyboard:"wordListController")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
}
