//
//  textCollCe;;.swift
//  newCamera
//
//  Created by WKC on 2016/12/14.
//  Copyright © 2016年 WKC. All rights reserved.
//

import Foundation


protocol textCollProtocol{
    func deleteText(path:String!)
    func alert(text:String!)
}

class textCollCell:UICollectionViewCell{
    
    
    var text:String!
    var image:UIImage!
    var title:String!
    var lang:String!
    var path:String!
    var colorList:[String:UInt] = ["en":0xF9CD42,"es":0xFFA938,"fr":0xF6A502,"zh-CHS":0xFF5C74,"ko":0xFF0000,"de":0xC7FF5F,"ru":0x95FF5F,"it":0x2EC8A5]
    @IBOutlet weak var textTitle: UILabel!
    @IBOutlet weak var textImageView: UIImageView!
    @IBOutlet weak var backLabel: UILabel!
    @IBOutlet weak var dltButton: UIButton!
    
    var delegate:textCollProtocol?
    func setCell(textInfo:textSet!){
        self.text = textInfo.contentText
        self.title = textInfo.titleText
        self.lang = textInfo.lang
        self.image = textInfo.titleImage
        self.path = textInfo.path
        self.dltButton.setImage(UIImage(named:"rub.png"), for: .normal)
        self.dltButton.tintColor = UIColor.white
        self.dltButton.addTarget(self, action: "deleteText", for: .touchUpInside)
        self.textTitle.text = textInfo.titleText
        self.textTitle.font = UIFont.boldSystemFont(ofSize: UIFont.labelFontSize)
        self.textTitle.textAlignment = .center
        self.textTitle.textColor = UIColorFromRGB(0xFF3399)//UIColor.white
       // let i:Int!  = (Int)(arc4random_uniform(UInt32(colorList.count)))
        print("言語=>\(lang)")
        var colorCode = colorList[lang]
        self.textTitle.textColor = UIColor.white//UIColorFromRGB(colorCode!)
        self.textImageView.image = textInfo.titleImage
        self.backLabel.alpha = 0.7
        self.backLabel.backgroundColor = UIColorFromRGB(colorCode!)
    }
    
    func deleteText(){
        delegate?.deleteText(path: self.path)
        delegate?.alert(text: self.text)
    }
    
    
    func UIColorFromRGB(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(0.8)
        )
    }
    
}
