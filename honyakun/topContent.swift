//
//  topContent.swift
//  honyakun
//
//  Created by WKC on 2017/02/26.
//  Copyright © 2017年 WKC. All rights reserved.
//

import Foundation


class topContent{
    var imageName:String!
    var contentTitle:String!
    
    init(imageName:String,contentTitle:String){
        self.imageName = imageName
        self.contentTitle = contentTitle
    }
}
