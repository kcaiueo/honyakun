//
//  wordPropertyController.swift
//  honyakun
//
//  Created by WKC on 2017/04/04.
//  Copyright © 2017年 WKC. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class wordPropertyController:UIViewController{
    
    
    var worddb = wordDB.accessDB//wordDB()
    
    var word:String!
    var lang:String!
    var exText:String!
    var trans:String!
    
    var wordLabel:UILabel!
    var exTextView:UITextView!
    var transView:UITextView!
    var megahon:UIButton!
    
    var talker = AVSpeechSynthesizer()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        setTopView()
        setProperty()
    }
    
    func setProperty(){
        var info = worddb.getAnsWord(wordw: word)
        wordLabel = UILabel(frame:CGRect(x:20,y:80,width:150,height:40))
        wordLabel.text = word
        wordLabel.font = UIFont.systemFont(ofSize: CGFloat(20))
        wordLabel.textColor = UIColor.black
        self.view.addSubview(wordLabel)
        
        var line = UILabel(frame:CGRect(x:0,y:125,width:self.view.frame.size.width,height:1))
        line.backgroundColor = UIColorFromRGB(0xFF3399)
        self.view.addSubview(line)
        
        var closedButton = UIButton(frame:CGRect(x:50,y:self.view.frame.size.height-70,width:30,height:30))
        closedButton.addTarget(self, action: "turnback", for: .touchUpInside)
        closedButton.setImage(UIImage(named:"Closed.png"), for: .normal)
        self.view.addSubview(closedButton)
        
        var flag = UIImageView(frame:CGRect(x:self.view.frame.size.width-50,y:75,width:40,height:40))
        flag.image = UIImage(named:"flagred.png")
        self.view.addSubview(flag)
        
        transView = UITextView(frame:CGRect(x:20,y:140,width:self.view.frame.size.width-40,height:self.view.frame.size.height-220))
        transView.text = info
        transView.isUserInteractionEnabled = false
        transView.font = UIFont.boldSystemFont(ofSize: UIFont.labelFontSize)
        transView.textColor = UIColorFromRGB(0x888888)
        self.view.addSubview(transView)
        
        megahon = UIButton(frame:CGRect(x:self.view.frame.size.width/2-15,y:self.view.frame.size.height-70,width:30,height:30))
        megahon.setImage(UIImage(named:"megaphone.png"), for: .normal)
        megahon.addTarget(self, action: "Wsay", for: .touchUpInside)
        self.view.addSubview(megahon)
        
    }
    
    
    func Wsay(){
        let utterance = AVSpeechUtterance(string:self.word as String)
        // 言語を日本に設定
        utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        // 実行
        utterance.rate = (0.3)//AVSpeechUtteranceMinimumSpeechRate
        utterance.pitchMultiplier = 1.5
        self.talker.speak(utterance)
        
        
    }
    
    
    
    
    func setTopView(){
        let naviVie:UIView = UIView(frame:CGRect(x: 0,y: 0,width: self.view.frame.size.width,height: 20))
        naviVie.backgroundColor = UIColorFromRGB(0xFFFFF3)
        self.view.addSubview(naviVie)
        let naviView:UIView = UIView(frame:CGRect(x: 0,y: 20,width: self.view.frame.size.width,height: 45))
        naviView.backgroundColor = UIColorFromRGB(0xFFFFF3)
        
        let navLabel:UILabel = UILabel(frame:CGRect(x: self.view.frame.size.width/3,y: 0,width: self.view.frame.size.width/3,height: 45))
        navLabel.text="単語詳細"
        navLabel.textColor = UIColorFromRGB(0xFF5F5C)
        navLabel.textAlignment = NSTextAlignment.center
        navLabel.font=UIFont.boldSystemFont(ofSize: UIFont.labelFontSize)//
        navLabel.backgroundColor=UIColorFromRGB(0xFFFFF3)
        UIFont.systemFont(ofSize: UIFont.buttonFontSize)
        
        let tbutton = UIButton(frame:CGRect(x: 7.5,y: 7.5,width: 30,height: 30))
        let image = UIImage(named:"la.png")
        let size = CGSize(width: 30, height: 30)
        UIGraphicsBeginImageContextWithOptions(size,false,0.0)
        image!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let resizeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        tbutton.setImage(resizeImage, for: UIControlState())
        tbutton.addTarget(self,action:"turnback",for:.touchUpInside)
        naviView.addSubview(tbutton)
        naviView.addSubview(navLabel)
        self.view.addSubview(naviView)
        
    }
    
    func turnback(){
        self.navigationController?.popViewController(animated: true)
        //self.dismiss(animated: true, completion: nil)
    }

    

}
