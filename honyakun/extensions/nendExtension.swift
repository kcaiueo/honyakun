//
//  nendExtension.swift
//  honyakun
//
//  Created by 和田継嗣 on 2017/04/30.
//  Copyright © 2017年 WKC. All rights reserved.
//

import Foundation
import NendAd



extension UIViewController{
    func adShow() {
        var adCount = Int(arc4random_uniform(100))
        if(adCount > 40){
            return
        }
        let showResult = NADInterstitial.sharedInstance().showAd(from: self)
        switch(showResult){
        case .AD_SHOW_SUCCESS:
            print("広告の表示に成功しました。")
            break
        case .AD_SHOW_ALREADY:
            print("既に広告が表示されています。")
            break
        case .AD_FREQUENCY_NOT_REACHABLE:
            print("広告のフリークエンシーカウントに達していません。")
            break
        case .AD_LOAD_INCOMPLETE:
            print("抽選リクエストが実行されていない、もしくは実行中です。")
            break
        case .AD_REQUEST_INCOMPLETE:
            print("抽選リクエストに失敗しています。")
            break
        case .AD_DOWNLOAD_INCOMPLETE:
            print("広告のダウンロードが完了していません。")
            break
        case .AD_CANNOT_DISPLAY:
            print("指定されたViewControllerに広告が表示できませんでした。")
            break
        }
    }

}
