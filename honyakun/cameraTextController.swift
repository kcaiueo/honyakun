//
//  cameraTextController.swift
//  honyakun
//
//  Created by WKC on 2016/09/07.
//  Copyright © 2016年 WKC. All rights reserved.
//

import Foundation

import UIKit
import Alamofire
import GoogleMobileAds
import NendAd

class cameraTextController: UIViewController,UIWebViewDelegate,popWordProt,touchImageViewProtocol,URLSessionTaskDelegate,GADBannerViewDelegate,cameraHttpProtocol,NADViewDelegate{
    
    //画像
    var im:UIImage!
    var crip:Int!
    //ナビゲーションバー高さ
     var barHeight:CGFloat!
    
    //webView
    var ocrView:UIWebView!
    var webText:String! = ""
    var htmlContents:String!
    var onlyTextContents = ""
    
    //選択された単語の言語
    var lang:String!
    
    //単語カード
    var wordView:popWord!
    
    //保存、リクエスト周辺
    var TouchedWord = ""

    //発音クラス
    var sayClass:SayClass!
    
    //保存ボタン
    var preserveButton:UIBarButtonItem!
    
    //本テキスト
    var orgText:String!
    
    //画蔵保存用パス
    var imagePath: String {
        let doc = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        return doc
    }

    //文書画像周り
    var textImageView:touchImageView!
    var textImageButton:UIBarButtonItem!
    var textImageViewFlag:Bool = false
    
    //db
    var worddb = wordDB.accessDB//wordDB()
    var textdb = textDB.accessDB//textDB()
    var imagedb = imageDB.accessDB//imageDB()
    var orgdb = orgTextDB.accessDB//orgTextDB()
    var iddb = IDCheckDB.accessDB//IDCheckDB()
    
    //API
      var cameraRequest:CameraRequestAPI!
    


    //広告周り
    var bannerView: GADBannerView!
    var wCount = 0
    var nadView:NADView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        let tmpImageView = UIImageView(frame: CGRect(x: 0,y: 0,width: self.view.frame.size.width,height: self.view.frame.size.height-60))
        tmpImageView.image = im
        setSubView()
        setNabVar()
        ocrText()
        setCameraRequestAPI()
        wCount = Int(arc4random_uniform(100))
        adNend()
       // self.adCheck()
    }
    
    func setCameraRequestAPI(){
        cameraRequest = CameraRequestAPI(lang: lang)
        cameraRequest.delegate = self
    }

    
    func setNabVar(){
        // タイトルを設定する.
        self.navigationItem.title = "honyakun"
        let titleView = UILabel(frame:CGRect.zero)
        titleView.font = UIFont.boldSystemFont(ofSize: 14.0)
        titleView.textColor = UIColorFromRGB(0xFF5F5C)
        titleView.text = "honyakun"
        titleView.sizeToFit()
        self.navigationItem.titleView = titleView
        
    }
    
    func setSubView(){
        
        
        //発音クラス初期化
        sayClass = SayClass(lang:lang)
        
        //戻るボタン
        var exit:UIButton!
        exit = UIButton(frame:CGRect(x: self.view.frame.size.width/10,y: self.view.frame.size.height-50,width: self.view.frame.size.width/5,height: 40))
        exit.setTitle("戻る",for:UIControlState())
        exit.addTarget(self, action: #selector(cameraTextController.disView), for: UIControlEvents.touchUpInside)
        exit.setTitleColor(UIColorFromRGB(0xFF0000), for: UIControlState())
        exit.tintColor=UIColorFromRGB(0xFF0000)
        self.view.addSubview(exit)
        
        //webView
        self.barHeight = self.navigationController?.navigationBar.frame.size.height
        ocrView = UIWebView(frame:CGRect(x: 0,y: (self.navigationController?.navigationBar.frame.size.height)!+20,width: self.view.frame.size.width,height: self.view.frame.size.height-barHeight))
        ocrView.delegate=self
        // ocrView.scalesPageToFit = true;
        ocrView.backgroundColor=UIColor.white
         SVProgressHUD.show(withStatus: "解析中")
        self.view.addSubview(ocrView)
        
        //単語View
        wordView = popWord(frame: CGRect(x: 0,y: self.view.frame.size.height+30,width: self.view.frame.size.width,height: 200),word: "",text: "",lang:lang)
        wordView.delegate = self
        self.view.addSubview(wordView)
        
        
        //navButton
        let size = CGSize(width: 20, height: 20)
        let tmpImage = UIImage(named: "new-document.png")
        let textImage = UIImage(named: "album.png")
        
        let navDocImage = resizeImage(tmpImage!,size: size)
        let textRImage = resizeImage(textImage!, size: size)
        
        //文書画像ボタン
        textImageButton = UIBarButtonItem(image:textRImage, style: UIBarButtonItemStyle.plain, target: self, action: #selector(cameraTextController.slideImage))
        
        //保存ボタン
        preserveButton =  UIBarButtonItem(image:navDocImage, style: UIBarButtonItemStyle.plain, target: self, action: #selector(cameraTextController.saveImage))
        
        self.navigationItem.setRightBarButtonItems([preserveButton,textImageButton], animated:true)
        
        //文書画像View
        textImageView = touchImageView(frame:CGRect(x: self.view.frame.size.width,y: (self.navigationController?.navigationBar.frame.size.height)!,width: self.view.frame.size.width,height: self.view.frame.size.height-(self.navigationController?.navigationBar.frame.size.height)!))
        textImageView.delegates = self
        textImageView.isUserInteractionEnabled = true
        textImageView.image = self.im
        self.view.addSubview(textImageView)
    }
    
    
    func disView(){
        self.navigationController?.popViewController(animated: true)
        for subview in (self.navigationController?.navigationBar.subviews)!{
            /*if(subview == "UIButton"){
                subview.removeFromSuperview()
            }*/
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        //self.navigationController?.navigationBar.backgroundColor = UIColorFromRGB(0xF8F8F8)
        

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}


extension cameraTextController{
    func dawnView() {
        wordView.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height+100)
    }
    
    func wordPreserve(_ word:String,text:String) {
        //_mySwiftData.wordAdd(word, str2:text,lang:lang)
        self.worddb.saveWord(wordw: word,mean: text,lang: lang)
        SVProgressHUD.showSuccess(withStatus: "追加しました!")
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        SVProgressHUD.dismiss()
        
        let kScheme = "native://";
        let url = request.url!.absoluteString
        //  println("aiueo")
        if url.hasPrefix(kScheme) {
            // Do something
            // println("翻訳取得開始")
            cameraRequest.dealRequest(request)
             //self.adCheck()
            //dealRequest(request)
            return false  // ページ遷移を行わないようにfalseを返す
        }
        return true
    }

}

extension cameraTextController{
    
    func ocrText(){
        let img = self.im//UIImage(named: "image.jpg")!
        let url  = URL(string:"http://153.126.180.38/camerakun/api/ocr/parameters?lang=\(self.lang!)/post")
        let headers = HTTPHeaders.init()
        let reqs = try! URLRequest(url: url!, method: .post, headers: headers)
        let parameters = ["text":"I like sushi"]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            let fileData = UIImageJPEGRepresentation(img!, 1.0)
            multipartFormData.append(fileData!, withName: "gazo", fileName: "imagev.jpg", mimeType: "image/jpeg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, with:reqs,encodingCompletion: { (result) in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    var data = response.data
                    do{
                        let json:NSDictionary = try JSONSerialization.jsonObject(with: data!,
                                                                                 options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        var str = json["content"] as! String
                        var org = json["org"] as! String
                        self.onlyTextContents = org
                        str = self.convertSpecialCharacters(str)
                        self.OcrView(str)
                    }catch{
                        self.OcrView("解析に失敗しました。撮影環境を変えて撮影してみてください。")
                        SVProgressHUD.dismiss()
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
            
        })
    }
    
    
    func OcrView(_ text:String){
        self.webText = text
        self.ocrView.loadHTMLString(text, baseURL: nil)
        SVProgressHUD.dismiss()
    }
    
    
    func convertSpecialCharacters(_ string: String) -> String {
        var newString = string
        let char_dictionary = [
            "&amp;": "&",
            "&lt;": "<",
            "&gt;": ">",
            "&quot;": "\"",
            "&apos;": "'"
        ];
        for (escaped_char, unescaped_char) in char_dictionary {
            newString = newString.replacingOccurrences(of: escaped_char, with: unescaped_char, options: NSString.CompareOptions.regularExpression, range: nil)
        }
        return newString
    }
}


extension cameraTextController{
    func popWordView(word:String,meanText text:String,type:String){
        self.wordView.setProperty(word, text: text)
        adShowView()
       // adCheck()
        //アニメーション
        UIView.animate(withDuration: 0.5,
                       
                       // 遅延時間.
            delay: 0.0,
            
            // バネの弾性力. 小さいほど弾性力は大きくなる.
            usingSpringWithDamping: 0.7,
            
            // 初速度.
            initialSpringVelocity: 0.5,
            
            // 一定の速度.
            options: UIViewAnimationOptions.curveLinear,
            
            animations: { () -> Void in
                
                self.wordView.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-100)
                
                // アニメーション完了時の処理
        }) { (Bool) -> Void in
            
        }
        
    }
    
    
    func showLoading(type: String) {
        if(type=="word"){
            SVProgressHUD.show(withStatus: "単語取得中")
        }else{
            SVProgressHUD.show(withStatus: "翻訳中")
        }
    }
    
    func deleteLoading() {
        SVProgressHUD.dismiss()
    }
}



extension cameraTextController{
    func saveImage(){
        
        var titleText=""
        //日付を取得し、ファイル名を獲得
        let image = self.im
        let data = UIImageJPEGRepresentation(image!, 1.0)
        let now = Date() // 現在日時の取
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        var comps = (0, 0, 0, 0)
        (calendar as NSCalendar).getEra(&comps.0, year: &comps.1, month: &comps.2, day: &comps.3,from:now)
        var comps2 = (0,0,0,0)
        (calendar as NSCalendar).getHour(&comps2.0, minute: &comps2.1, second: &comps2.2, nanosecond: &comps2.3, from: now)
        let str = "\(comps.0)\(comps.1)\(comps.2)\(comps.3)\(comps2.0)\(comps2.1)\(comps2.2)\(comps2.3).png"
        //
        let filepath:String! = URL(fileURLWithPath:imagePath).appendingPathComponent(str).path
        
        let myAlert: UIAlertController = UIAlertController(title: "保存", message: "解析結果を保存します。", preferredStyle: .alert)
        
        // OKのアクションを作成する.
        //let myOkAction = UIAlertAction(title: "OK", style: .Default) { action in}
        let myOkAction:UIAlertAction = UIAlertAction(title: "OK",
                                                     style: UIAlertActionStyle.default,
                                                     handler:{
                                                        (action:UIAlertAction!) -> Void in
                                                        let textFields:Array<UITextField>? =  myAlert.textFields as Array<UITextField>?
                                                        if textFields != nil {
                                                            for textField:UITextField in textFields! {
                                                                //各textにアクセス
                                                                titleText = textField.text!
                                                                try? data!.write(to: URL(fileURLWithPath: filepath), options: [.atomic])
                                                               
                                                                self.imagedb.addImage(name: str, title: titleText, text: self.webText!, lang: self.lang)
                                                            
                                                                self.orgdb.addImage(name: str, title: titleText, text: self.onlyTextContents,lang:self.lang)
                                                                //self._mySwiftData.addOrgText(str,title:titleText,text:self.orgText!,lang: self.lang)
                                                                //self._mySwiftData.
                                                                SVProgressHUD.showSuccess(withStatus: "保存完了!")
                                                                
                                                                
                                                            }
                                                        }
        })
        
        let cancelAction:UIAlertAction = UIAlertAction(title: "Cancel",
                                                       style: UIAlertActionStyle.cancel,
                                                       handler:{
                                                        (action:UIAlertAction!) -> Void in
                                                        //SVProgressHUD.showSuccessWithStatus("保存完!")
                                                        
        })
        
        
        // OKのActionを追加する.
        myAlert.addAction(myOkAction)
        myAlert.addAction(cancelAction)
        myAlert.addTextField(configurationHandler: {(text:UITextField!) -> Void in
            text.placeholder = "first textField"
        })
        
        // UIAlertを発動する.
        present(myAlert, animated: true, completion: nil)

    }
    
    func slideImage(){
        popImageView()
    }
    
    func popImageView(){
        
        if(self.textImageViewFlag == false){
            //アニメーション
            UIView.animate(withDuration: 0.5,
                                       
                                       // 遅延時間.
                delay: 0.0,
                
                // バネの弾性力. 小さいほど弾性力は大きくなる.
                usingSpringWithDamping: 0.8,
                
                // 初速度.
                initialSpringVelocity: 0.5,
                
                // 一定の速度.
                options: UIViewAnimationOptions.curveLinear,
                
                animations: { () -> Void in
                    self.textImageView.layer.position = CGPoint(x: self.view.frame.size.width/2+80, y: (self.navigationController?.navigationBar.frame.size.height)!+self.view.frame.size.height/2)
                    self.textImageViewFlag = true
                    
                    // アニメーション完了時の処理
            }) { (Bool) -> Void in
                
            }
        }else{
            
            self.textImageView.layer.position = CGPoint(x: self.view.frame.size.width+2000, y: (self.navigationController?.navigationBar.frame.size.height)!+self.view.frame.size.height/2);
            self.textImageViewFlag = false
        }
    }
    
    func textImageTouched() {
        popImageView()
    }
    
    func adCheck(){
        if bannerView == nil{
            bannerView = GADBannerView(adSize:kGADAdSizeFullBanner,origin: CGPoint(x: 0,y: self.view.frame.size.height-kGADAdSizeFullBanner.size.height))
        }
        if(wCount>50){
            
            // bannerView.frame = CGRectMake(0,self.view.frame.size.height-bannerView.frame.size.height,self.view.frame.size.width,bannerView.frame.size.height)
            // AdMobで発行された広告ユニットIDを設定
            bannerView.adUnitID = "ca-app-pub-6722210748079586/9275127954"
            bannerView.delegate = self
            bannerView.rootViewController = self
            let gadRequest:GADRequest = GADRequest()
            //gadRequest.testDevices = ["af78212ff0e303a248c3725a11bd7c6b"]
            // テスト用の広告を表示する時のみ使用（申請時に削除）
            bannerView.load(gadRequest)
            self.view.addSubview(bannerView)
        }else{
            //  bannerView = GADBannerView(adSize:kGADAdSizeFullBanner,origin: CGPointMake(0,self.view.frame.size.height-kGADAdSizeFullBanner.size.height))
            bannerView.removeFromSuperview()
        }
        
    }

    
    func resizeImage(_ image:UIImage,size:CGSize)->UIImage{
        
        UIGraphicsBeginImageContextWithOptions(size,false,0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let resizeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resizeImage!
        
    }

}


extension cameraTextController{
    func adNend(){
        var magnification = self.view.frame.size.width / 320
        magnification = 50 * magnification
        // 広告枠作成
        nadView = NADView(frame: CGRect(x:0, y:self.view.frame.size.height - magnification , width:self.view.frame.size.width ,height:magnification), isAdjustAdSize: true)
        // 広告枠のapikey/spotidを設定(必須)
        // NADInterstitial.sharedInstance().loadAd(withApiKey: "a6eca9dd074372c898dd1df549301f277c53f2b9", spotId: "3172")
        nadView.setNendID("5c33a84a69b8bfd8145ccf55efd0e2547c03b2a1",spotID: "745795")

        // nendSDKログ出力の設定(任意)
        nadView.isOutputLog = false
        // delegateを受けるオブジェクトを指定(必須)
        nadView.delegate = self
        // 読み込み開始(必須)
        nadView.load()
        // 通知有無にかかわらずViewに乗せる場合
        // self.view.addSubview(nadView)
    }
    
    func adShowView(){
        var adCount = Int(arc4random_uniform(100))
        if(adCount > 30){
            nadView.pause()
            // nadView.layer.position = CGPoint(x:0,y:self.view.frame.size.height+100)
            nadView.removeFromSuperview()
            // nadView.isHidden = true
        }else{
            var magnification = self.view.frame.size.width / 320
            magnification = 50 * magnification
            //nadView.layer.position = CGPoint(x:0,y:self.view.frame.size.height - magnification)
            //nadView.
            // nadView = ::NADView(frame: CGRect(x:0, y:self.view.frame.size.height - magnification , width:self.view.frame.size.width ,height:magnification), isAdjustAdSize: true)
            nadView.resume()
            self.view.addSubview(nadView)
        }
    }
}



