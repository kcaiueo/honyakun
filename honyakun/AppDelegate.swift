//
//  AppDelegate.swift
//  honyakun
//
//  Created by WKC on 2016/09/03.
//  Copyright © 2016年 WKC. All rights reserved.
//

import UIKit
import NendAd
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let cont = instantiate(TopListController.self,storyboard:"TopListController")//instantiate(newTopViewController.self,storyboard:"newTopController") //TopListController()instantiate(newTopViewController.self,storyboard:"newTopController")
        let navCon =  UINavigationController(rootViewController: cont)
        SVProgressHUD.setBackgroundColor(UIColor.white)
        SVProgressHUD.displayDuration(for: "0.3")
        navCon.navigationBar.barTintColor = UIColorFromRGB(0xFFFFF3)
        navCon.navigationBar.alpha = 1.0
   
       navCon.navigationBar.tintColor = UIColorFromRGB(0xFF5F5C)
       window?.rootViewController = navCon
        NADInterstitial.sharedInstance().loadAd(withApiKey: "c957cd2811c497188d2d0ed08118152f149e3a71", spotId: "744113")
        NADInterstitial.sharedInstance().isOutputLog = true

       return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func UIColorFromRGB(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }



}

