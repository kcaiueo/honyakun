//
//  topContentCell.swift
//  honyakun
//
//  Created by WKC on 2017/02/26.
//  Copyright © 2017年 WKC. All rights reserved.
//

import Foundation


class topContentCell:UITableViewCell{
    
    
    
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
    }
    
    func setCell(_ topcontent:topContent){
        self.iconImageView.image = UIImage(named:topcontent.imageName)
        self.titleLabel.text = topcontent.contentTitle
        let separator = UIView(frame:CGRect(x:0,y:0,width:UIScreen.main.bounds.width,height:0.5))
        separator.backgroundColor = UIColorFromRGB(0xEEEEEE)
        self.addSubview(separator)
    }
    
    func resizeImage(_ image:UIImage,size:CGSize)->UIImage{
        UIGraphicsBeginImageContextWithOptions(size,false,0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let resizeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resizeImage!
        
    }
    
    func UIColorFromRGB(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

}
